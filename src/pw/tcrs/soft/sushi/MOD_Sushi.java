package pw.tcrs.soft.sushi;

import java.util.logging.Level;

import asia.tcrs.curry.VillagerTrade;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.Property;
import pw.tcrs.cookcore.Foods.TCRSCookCoreFoods;
import pw.tcrs.cookcore.tools.TCRSCookCoreTools;
import pw.tcrs.tcrscore.TcrsCore;
import pw.tcrs.tcrscore.sum.IMOD;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.common.registry.VillagerRegistry;

@Mod(modid="TCRS_Sushi", name="TCRS Sushi", version="Beta1.0", dependencies = "required-after:TcrsCore2.5;required-after:TCRSCookCoreFoods")
@NetworkMod(clientSideRequired=true,serverSideRequired=false)
public class MOD_Sushi implements IMOD{
	
	public static Item Syari;
	public static Item maguro_saku;
	public static Item maguro_kirimi;
	public static Item maguro_nigiri;
 
	protected int ITEM_ID_RANGE=10;
	private int ItemBaseID;
	private int[] ItemID = new int[ITEM_ID_RANGE];
	
	public static SVillagerTrade villager;

	@Override
	@EventHandler
	public void preload(FMLPreInitializationEvent Event) {
		
		Configuration cfg = new Configuration(TcrsCore.getconfigfile("SushiMOD"));
		try
		{
			cfg.load();
			Property ItemStartIDProp  = cfg.getItem(Configuration.CATEGORY_ITEM,"ItemID", 10100);
			ItemBaseID  = ItemStartIDProp.getInt();
		}
		catch (Exception e)
		{
			FMLLog.log(Level.SEVERE, e, "Error Message");
		}
		finally
		{
			cfg.save();
		}
		for(int i=0; i<ITEM_ID_RANGE; i++) {
	        ItemID[i] = ItemBaseID +i;
		}
		
		registerItem();
		
		
	}

	@Override
	public void registerItem() {
		Syari = new ItemFood(ItemID[0], 2, false).setUnlocalizedName("tcrssushi:syari").setTextureName("tcrssushi:syari")
				.setMaxStackSize(64).setCreativeTab(CreativeTabs.tabFood);
		maguro_saku = new Item(ItemID[1]).setUnlocalizedName("tcrssushi:maguro_saku").setTextureName("tcrssushi:maguro_saku")
				.setMaxStackSize(16).setCreativeTab(CreativeTabs.tabFood);
		maguro_kirimi = new Item(ItemID[2]).setUnlocalizedName("tcrssushi:maguro_kirimi").setTextureName("tcrssushi:maguro_kirimi")
				.setMaxStackSize(64).setCreativeTab(CreativeTabs.tabFood);
		maguro_nigiri = new ItemFood(ItemID[3], 4, false).setUnlocalizedName("tcrssushi:maguro_nigiri").setTextureName("tcrssushi:maguro_nigiri")
				.setMaxStackSize(64).setCreativeTab(CreativeTabs.tabFood);
		
	}

	@Override
	@EventHandler
	public void load(FMLInitializationEvent event) {
		LanguageRegistry.addName(Syari, "Syari");
		LanguageRegistry.addName(maguro_saku, "maguro(saku)");
		LanguageRegistry.addName(maguro_kirimi, "maguro fillets");
		LanguageRegistry.addName(maguro_nigiri, "maguro nigiri");
		
		villager = new SVillagerTrade();
		/*
		 * 村人の取引内容を登録しています(村人の職業ID,IVillageTradeHandlerを実装したクラス)
		 */
		VillagerRegistry.instance().registerVillageTradeHandler(4, villager);
		
		GameRegistry.addShapelessRecipe(new ItemStack(maguro_kirimi, 15), new Object[]{
			new ItemStack(TCRSCookCoreTools.Houtyou),maguro_saku});
		GameRegistry.addShapelessRecipe(new ItemStack(Syari, 10), new Object[]{
			new ItemStack(TCRSCookCoreFoods.Potricecookedtoaturn),TCRSCookCoreFoods.SusiSu});
		GameRegistry.addShapelessRecipe(new ItemStack(maguro_nigiri), new Object[]{
			new ItemStack(maguro_kirimi),Syari});
		
	}

}
