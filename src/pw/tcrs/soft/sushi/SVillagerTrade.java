package pw.tcrs.soft.sushi;

import java.util.Random;

import pw.tcrs.cookcore.Foods.TCRSCookCoreFoods;

import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.village.MerchantRecipe;
import net.minecraft.village.MerchantRecipeList;
import cpw.mods.fml.common.registry.VillagerRegistry.IVillageTradeHandler;


public class SVillagerTrade implements IVillageTradeHandler {

	@Override
	public void manipulateTradesForVillager(EntityVillager villager,
			MerchantRecipeList recipeList, Random random) {
		if(villager.getProfession() == 4) {
		/*
		 * エメラルド１個を村人に売って米一升買う取引を登録
		 */
		recipeList.add(new MerchantRecipe( new ItemStack(Item.emerald, 1, 0), TCRSCookCoreFoods.Rice1syou));
		recipeList.add(new MerchantRecipe( new ItemStack(Item.emerald, 1, 0), TCRSCookCoreFoods.SusiSu));
		
		recipeList.add(new MerchantRecipe( new ItemStack(Item.emerald, 1, 0), MOD_Sushi.maguro_saku));

	}
		
	}

}

